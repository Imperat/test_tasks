import pytest
from main import brackets_preparer, brackets_preparer_regular
class TestBracketsPreparer(object):
    tests = {
        '': '',
        '(a)': '(a)',
        'abc': 'abc',
        '(ab)(cd)': '(ab)(cd)',
        '(ab(cd))': '(ab(cd))',
        '(ab(cd)e)': '(ab(cd)e)',
        'ab(cd': 'ab',
        '(abc': '',
        'sdfd((esdf)(esdf': 'sdfd((esdf)',
        '(': '', 
        '((':'',
        '(()))(':'(()))'  
    }
    def test_brackets_preparer(self):
        for inp, res in self.tests.items():
            assert brackets_preparer(inp) == res
    def test_brackets_preparer_regular(self):
        for inp, res in self.tests.items():
            assert brackets_preparer_regular(inp) == res