import re
def brackets_preparer(string):
    open_position = string.rfind('(')
    close_position = string.rfind(')')
    if open_position > close_position:
        return brackets_preparer(string[:open_position])
    return string
def brackets_preparer_regular(string):
    return re.sub(r'\([^\)]*$', '', string)